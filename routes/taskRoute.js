// Defines WHEN particular controllers will be used 
// Contain all the endpoints and responses that we can get from controllers


const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController");

// Route to get all the tasks
// http://localhost:3001/tasks        // from index it is already declared
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});


router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

//// [SECTION] ACTIVITY  ///////////////////////

router.get("/:id", (req, res) => {
	taskController.getTaskById(req.params.id).then(resultFromController => res.send(resultFromController));
});


router.put("/:id/complete", (req, res) => {
	taskController.updateTask(req.params.id).then(resultFromController => res.send(resultFromController));
});


module.exports = router;


