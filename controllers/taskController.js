// Contain instructions on HOW your API will perform its intended tasks
// All the operations it can do will be placed in this file

const mongoose = require("mongoose");


const Task = require("../models/task");

//Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

//Controller function for creating a task
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name

		
	})

	return newTask.save().then((task, error) => {
		if (error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
};

/*Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err){
			console.log(err);
			return false;
		} 
			else 
		{
			return "Deleted Task ehh"
		}
	})
};


//// [SECTION] Activity

module.exports.getTaskById = (taskId) => {



	  return Task.findById(taskId).then((task, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return task;
    }
  });
};


module.exports.updateTask = (taskId) => {
	return Task.findByIdAndUpdate(taskId, { status: "complete"}, { new: true }).then((removedTask, err) => {
		if (err){
			console.log(err);
			return false;
		} 
			else 
		{
			
			return removedTask
		}
	})
};


